package org.cuatrovientos.java.operators;

public class MultiplyArguments {
	/*
	 * Ejercicio 6: Crea un proyecto con una clase llamada MultiplyArguments que
	 * recoja dos valores como argumentos (arg[0] y arg[1]), los multiplique y
	 * muestre el resultado por pantalla.
	 */
	public static void ejercicio6(String args, String args2) {
		System.out.println("La multiplicacion de " + args + " por " + args2 + " es: "
				+ (Float.parseFloat(args) * Float.parseFloat(args2)));
	}
}
