package org.cuatrovientos.java.operators;

public class CheckOperatorResult {
	/*
	 * 				Ejercicio2
	 * ¿Que resultado se obtienen de las siguientes operaciones booleanas? 
	 * Crea un proyecto con una clase llamada CheckOperatorResult
	 * que lleve a cabo las siguientes operaciones. Ejecutalo en el depurador
	 * 
	 * 1. int x = 4;
	 * 2. int y = 6;
	 * 3. (x > 0);
	 * 4. (x > 0) || (y > 7);
	 * 5. (x <= 4) && (y != 4);
	 * 6. !((x < 4) && (y > 42));
	 * 7.!(x < 4) || !(y > 42);
	 * 8. x == 4 && y == 7;
	 */
	public static void ejercicio2() {
		int x = 4;
		int y = 6;
		System.out.println(x > 0);
		System.out.println((x > 0) || (y > 7));
		System.out.println((x <= 4) && (y != 4));
		System.out.println(!((x < 4) && (y > 42)));
		System.out.println(!(x < 4) || !(y > 42));
		System.out.println(x == 4 && y == 7);

	}

}
