package org.cuatrovientos.java.operators;

import java.util.Scanner;

public class CompleteName {
	/*
	 * Ejercicio 8: Crea un proyecto con una clase llamada CompleteName que solicite
	 * por consola el nombre y dos apellidos. El programa debe mostrar al final el
	 * nombre completo en este formato:
	 * "Apellido1 Apellido2, Nombre"
	 	Introduzca su nombre
		Eugenia
		Introduzca su primer apellido
		Pérez
		Introduzca su segundo apellido
		Martínez
		Su nombre completo es: Pérez Martínez, Eugenia
	 */
	private static Scanner sc;

	public static void ejercicio8() {
		String nombre, apellido1, apellido2;
		sc = new Scanner(System.in);
		System.out.println("Introduzca su nombre");
		nombre = sc.nextLine();
		System.out.println("Introduzca su primer apellido");
		apellido1 = sc.nextLine();
		System.out.println("Introduzca su segundo apellido");
		apellido2 = sc.nextLine();
		System.out.println("Su nombre completo es: " + apellido1 + " " + apellido2 + ", " + nombre);
	}
}
