package org.cuatrovientos.java.operators;

import java.util.Scanner;

public class Average {
	/*
	 * Ejercicio 3 
	 * Crea un proyecto con una clase llamada Average que solicite 5
	 * numeros al usuario y calcule la media de esos valores. Toma numeros enteros y
	 * luego comprueba si obtienes una media precisa.
	 * 
	 */
	private static Scanner sc;

	public static void ejercicio3() {
		int x, y, j, z, n;
		float media;
		sc = new Scanner(System.in);
		System.out.println("Introduzca cinco numeros enteros para calcular su media:");
		x = Integer.parseInt(sc.nextLine());
		y = Integer.parseInt(sc.nextLine());
		j = Integer.parseInt(sc.nextLine());
		z = Integer.parseInt(sc.nextLine());
		n = Integer.parseInt(sc.nextLine());
		media = (x + y + j + z + n) / 5.0f;
		System.out.println("La media de los cinco numeros introducidos es: " + media);
	}
}
