package org.cuatrovientos.java.operators;

import java.util.Scanner;

public class Converter {
	/*
	 * Ejercicio 4 
	 * Crea un proyecto con una clase llamada Converter que solicite por
	 * consola al usuario un valor en dolares y lo convierta a euros.
	 */
	private static Scanner sc;

	public static void ejercicio4() {

		float d, e, conversion;
		e = 0.85f;// El cambio de Dolar por Euro
		sc = new Scanner(System.in);
		System.out.println("Introduzca La cantidad de $ para su conversion a €:");
		d = Float.parseFloat(sc.nextLine());
		conversion = d * e;
		System.out.println("Sus " + d + " $ equivalen a " + conversion + " €");
	}
}
