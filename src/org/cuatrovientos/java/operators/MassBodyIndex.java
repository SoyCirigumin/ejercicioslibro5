package org.cuatrovientos.java.operators;

import java.util.Scanner;

public class MassBodyIndex {
	/*
	 * Ejercicio 7: Crea un proyecto con una clase llamada MassBodyIndex que
	 * solicite por consola dos valores. Los valores deben ser el peso en kilos y la
	 * altura en centimetros y debe calcularse el Indice de Masa Corporal: peso
	 * dividido por la altura al cuadrado. Luego multiplicalo por 10000 para sacar
	 * un numero mas legible por pantalla. Utiliza variables float para una mayor
	 * precision.
	 */
	private static Scanner sc;

	public static void ejercicio7() {
		float peso, altura, masa;
		sc = new Scanner(System.in);
		System.out.println("Introduzca el peso en Kilos");
		peso = Float.parseFloat(sc.nextLine());
		System.out.println("Introduzca la altura en Centimetros");
		altura = Float.parseFloat(sc.nextLine());
		masa = (float) ((peso / Math.pow(altura, 2)) * 10000);
		System.out.println("Su Indice de Masa Corporal es: " + masa);
	}
}
