package org.cuatrovientos.java.operators;

import java.util.Scanner;

public class Booleans {
	/*
	 * Ejercicio 5
	 * Crea un proyecto con una clase llamada Booleans que solicite al 
	 * usuario dos valores booleanos, lleve a cabo la comparacion and y or y muestre el resultado.
	 * La ejecucion podria ser asi:
	    Introduzca true/false
		true
		Introduzca true/false
		false
		AND operation
		true and false is: false
		OR operation
		true or false is: true
	 */
	private static Scanner sc;
	public static void ejercicio5() {
		boolean x, y;
		sc = new Scanner(System.in);
		System.out.println("Introduzca true/false");
		x = Boolean.parseBoolean(sc.nextLine());
		System.out.println("Introduzca true/false");
		y = Boolean.parseBoolean(sc.nextLine());
		System.out.println("AND operation");
		System.out.println(x + " and " + y + " is: " + (x && y));
		System.out.println("OR operation");
		System.out.println(x + " or " + y + " is: " + (x || y));
	}
}
