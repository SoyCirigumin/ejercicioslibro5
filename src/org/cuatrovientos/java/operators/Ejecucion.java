package org.cuatrovientos.java.operators;


public class Ejecucion {

	public static void main(String[] args) {
		System.out.println("Ejercicio1:");
		CheckOperators.ejercicio1();
		System.out.println("Ejercicio2:");
		CheckOperatorResult.ejercicio2();
		System.out.println("Ejercicio3:");
		Average.ejercicio3();
		System.out.println("Ejercicio4:");
		Converter.ejercicio4();
		System.out.println("Ejercicio5:");
		Booleans.ejercicio5();
		System.out.println("Ejercicio6:");
		MultiplyArguments.ejercicio6(args[0], args[1]);
		System.out.println("Ejercicio7:");
		MassBodyIndex.ejercicio7();
		System.out.println("Ejercicio8:");
		CompleteName.ejercicio8();
	}

}
