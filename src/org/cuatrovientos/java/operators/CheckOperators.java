package org.cuatrovientos.java.operators;

public class CheckOperators {
	/*
	 * Ejercicio1 ¿Que resultados se obtienen en las siguientes operaciones? Crea un
	 * proyecto con una clase llamada CheckOperators que lleve a cabo las siguientes
	 * operaciones y muestre el resultado. Si alguna es incorrecta, indicalo con un
	 * comentario y no la hagas. Ejecutalo en el depurardor.
	 * 	a. 10 - 7;
	 *  b. 40 + 2;
	 *  c. 10 * 4 + 2;
	 *  d. 56 % 4;
	 *  e. (100 / 4) + 25 - (16 / 2);
	 *  d. 9.5F + 3 * 100;
	 *  e. (2++)+5/3;
	 */
	public static void ejercicio1() {
		int resultado;
		resultado = 10 - 7;
		System.out.println(resultado);
		resultado = 40 + 2;
		System.out.println(resultado);
		resultado = 10 * 4 + 2;
		System.out.println(resultado);
		resultado = 56 % 4;
		System.out.println(resultado);
		resultado = (100 / 4) + 25 - (16 / 2);
		System.out.println(resultado);
		// Esto lo podriamos hacer definiendo otra variable de tipo float en lugar de int
		// resultado=39.5F+3*100;
		float resultado1 = 39.5F + 3 * 100;
		System.out.println(resultado1);
		/*
		 * falla resultado=(2++)+5/3;
		 */

	}

}
